package src.service;

import java.util.List;

import src.model.User;

public interface IUserService {

	
	public void addUser(User user);
	
	public User findById(Long id);

	public List<User> findAll();
	
	
}
