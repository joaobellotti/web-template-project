package src.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import src.dao.IUserDao;
import src.dao.UserDao;
import src.model.User;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class UserService implements IUserService{

	@Autowired
	private IUserDao userDao;
	
	
	@Override
	public void addUser(User user) {
		userDao.addUser(user);
	}

	@Override
	public User findById(Long id) {
		return userDao.findById(id);
	}

	public List<User> findAll() {
		return userDao.findAll();
	}

	

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	
	
}
