package src.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;


@Repository("entityManagerSupport")
public class EntityManagerSupport {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public EntityManager getEntityManager(){
		return entityManager;			
	}
	
//	public void save(DomainObject<?> entity){
//		if (entity.getId()==null){
//			this.insert(entity);
//		} else {
//			this.update(entity);
//		}
//	}
//	
//	public void insert(DomainObject<?> entity){
//		getEntityManager().persist(entity);
//		getEntityManager().flush();
//	}
//	
//	public void update(DomainObject<?> entity){
//		getEntityManager().merge(entity);
//		getEntityManager().flush();
//	}
	
	public void remove(Class<?> entityClass, Object id){
		Object entity = getEntityManager().find(entityClass,id);
		if (entity!=null){
			getEntityManager().remove(entity);		
			getEntityManager().flush();
		} else {
			throw new NoResultException(String.format("Registro [%s(id=%s)] n�o encontrado", entityClass.getSimpleName(),id ));
		}
	}
	
	public Object findById(Class<?> entityClass, Object id){
		try {
			return getEntityManager().find(entityClass, id);
		} catch (NoResultException e) {
		    return null;
		}
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@SuppressWarnings("rawtypes")
	public List findAll(Class<?> entityClass) {
		return getEntityManager().createQuery("select e from " + entityClass.getSimpleName() + " e").getResultList();
	}	 
//	
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public PagedList findAll(Class<?> entityClass, int first, int max) {
//		List result = getEntityManager().createQuery("select e from " + entityClass.getSimpleName() + " e").setFirstResult(first).setMaxResults(max).getResultList();
//
//		Long size = (Long)getEntityManager().createQuery("select count(e) from " + entityClass.getSimpleName() + " e").getSingleResult();
//		
//		return new PagedList(result, size.intValue(), first);	  
//	}	 
}
