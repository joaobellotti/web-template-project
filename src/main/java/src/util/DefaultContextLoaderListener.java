package src.util;


import static org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext;

import javax.servlet.ServletContextEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;


public class DefaultContextLoaderListener extends ContextLoaderListener {
	
    private static final Log logger = LogFactory.getLog(DefaultContextLoaderListener.class);
    
	public void contextInitialized(ServletContextEvent event) {
		logger.info("Initializing context.");
		
		super.contextInitialized(event);
		
        ApplicationContext context = getRequiredWebApplicationContext(event.getServletContext());
        
        doContextInitialization(context);
        
		logger.info("context initialized with success.");
	}
	
	protected void doContextInitialization(ApplicationContext context) {
	}

	public void contextDestroyed(ServletContextEvent event) {
		super.contextDestroyed(event);
	}

}
