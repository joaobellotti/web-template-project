package src.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.stereotype.Component;

import src.model.User;
import src.service.UserService;

@ManagedBean
@Component
@ViewScoped
public class UserBean {

	@ManagedProperty(value="#{userService}")
	private UserService userService ;
	
	private User user = new User();
	private List<User> usersList = new ArrayList<User>();

	@PostConstruct
	public void init(){
		usersList = userService.findAll();
	}
	
	
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String saveUser(User user){
		userService.addUser(user);
		return null;
	}

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}




	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	
}
