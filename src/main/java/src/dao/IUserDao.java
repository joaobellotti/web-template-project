package src.dao;

import java.util.List;

import src.model.User;

public interface IUserDao {

	
	public void addUser(User user);
	
	public User findById(Long id);

	public List<User> findAll();
	
	
}
