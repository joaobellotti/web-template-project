package src.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import src.model.User;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class UserDao extends AbstractDao implements IUserDao{

	
	
	public void addUser(User user) {
		getSessionFactory().getCurrentSession().persist(user);
	}

	@Override
	public User findById(Long id) {
		List list = getSessionFactory().getCurrentSession()
				.createQuery("from User where id=?")
		        .setParameter(0, id).list();
			return (User)list.get(0);
	}

	@Transactional
	public List<User> findAll() {
		List<User> list = getSessionFactory().getCurrentSession()
				.createQuery("from User").list();
			return list;
	}

	
	
}
